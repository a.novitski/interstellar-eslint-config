/* Customize for a specific project */
const imports = [
   // 'assets',
   'const',
   'components',
   'hooks',
   'modules',
   // 'navigation',
   'screens',
   'store',
   'utils',
 ];
 
 module.exports = {
   root: true,
   parser: '@typescript-eslint/parser',
 
   plugins: [
     '@typescript-eslint',
     'react',
     'react-native',
     'sort-destructure-keys',
     'typescript-sort-keys',
     'import',
   ],
 
   extends: [
     'eslint:recommended',
     'plugin:@typescript-eslint/recommended',
     'plugin:react/recommended',
     'plugin:react-native/all',
     'plugin:import/recommended',
     'plugin:import/typescript',
     '@react-native-community',
   ],
 
   settings: {
     'import/parsers': {
       '@typescript-eslint/parser': ['.ts', '.tsx'],
     },
     'import/resolver': {
       typescript: {
         alwaysTryTypes: true,
         project: './tsconfig.json',
       },
     },
   },
 
   rules: {
     /* ESLint */
     'arrow-body-style': ['warn', 'as-needed'],
     'no-shadow': 'off',
     'object-curly-newline': [
       'warn',
       {
         ObjectExpression: {
           minProperties: 1,
           multiline: true,
         },
         ObjectPattern: {
           consistent: true,
           multiline: true,
         },
         ImportDeclaration: {
           consistent: true,
           multiline: true,
         },
         ExportDeclaration: {
           multiline: true,
         },
       },
     ],
     'sort-imports': [
       'warn',
       {
         ignoreDeclarationSort: true,
       },
     ],
 
     /* typescript-eslint */
     '@typescript-eslint/consistent-type-imports': [
       'warn',
       {
         prefer: 'type-imports',
       },
     ],
     '@typescript-eslint/no-explicit-any': 'off',
     '@typescript-eslint/no-shadow': 'error',
     '@typescript-eslint/no-unused-vars': 'error',
 
     /* react */
     'react/jsx-no-bind': 'warn',
     'react/prop-types': 'off',
 
     /* react-hooks */
     'react-hooks/exhaustive-deps': 'warn',
 
     /* react-native */
     'react-native/sort-styles': [
       'warn',
       'asc',
       {
         ignoreClassNames: true,
         ignoreStyleProperties: false,
       },
     ],
 
     /* sort-destructure-keys */
     'sort-destructure-keys/sort-destructure-keys': [
       'warn',
       {
         caseSensitive: true,
       },
     ],
 
     /* typescript-sort-keys */
     'typescript-sort-keys/interface': [
       'warn',
       'asc',
       {
         caseSensitive: true,
         natural: true,
         requiredFirst: true,
       },
     ],
     'typescript-sort-keys/string-enum': [
       'warn',
       'asc',
       {
         caseSensitive: true,
         natural: true,
       },
     ],
 
     /* import */
     'import/default': 'off',
     'import/no-named-as-default-member': 'off',
     'import/order': [
       'warn',
       {
         groups: [
           'builtin',
           'external',
           'internal',
           'parent',
           'sibling',
           'index',
         ],
         pathGroups: [
           ...imports.map(dir => ({
             pattern: dir,
             group: 'internal',
           })),
           ...imports.map(dir => ({
             pattern: dir + '/**',
             group: 'internal',
           })),
         ],
         pathGroupsExcludedImportTypes: ['internal'],
         alphabetize: {
           order: 'asc',
           orderImportKind: 'desc',
           caseInsensitive: true,
         },
         'newlines-between': 'always',
       },
     ],
   },
 };
 